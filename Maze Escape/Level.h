#pragma once
#include <SFML/Graphics.hpp>
#include "Player.h"
#include "Wall.h"
#include <iostream>
#include <fstream>

class Level
{
public:
    Level(sf::Vector2f startingPos);
    void input();
    void update(sf::Time gameTime);
    void DrawTo(sf::RenderTarget& target);
    void LoadLevel(int levelNumber);
    
private:
    Player playerObject;
    std::vector<Wall> wallInstances;


};


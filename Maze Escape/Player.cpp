#include "Player.h"
#include <cmath>

Player::Player(sf::Vector2f startingPos)
	:AnimatingObject(AssetManager::RequestTexture("Assets/Graphics/playerAnimation.png"), 100, 100, 5)
    ,previousPosition (startingPos)
{
    addClip("walkDown", 0, 3);
    addClip("walkRight", 4, 7);
    addClip("walkUp", 8, 11);
    addClip("walkLeft", 12, 15);
    sprite.setPosition(startingPos);
    PlayClip("walkDown");
}

void Player::input()
{
    //Reset velocity at the start of each frame
    Velocity.x = 0.0f;

    Velocity.y = 0.0f;

    bool hasInput = false;

    //movement
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    {
        Velocity.x = speed;
        PlayClip("walkRight");
        hasInput = true;
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
    {
        Velocity.x = -speed;
        if (!hasInput) 
        {
            PlayClip("walkLeft");
        }
        
        hasInput = true;
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
        Velocity.y = speed;
        if (!hasInput)
        {
            PlayClip("walkDown");
        }
        hasInput = true;
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    {
        Velocity.y = -speed;
        if (!hasInput)
        {
            PlayClip("walkUp");
        }
        hasInput = true;
    }

    if (!hasInput)
    {
        Stop();
    }
}

void Player::update(sf::Time gameTime)
{
    //set the character to move when they have a velocity
    previousPosition = sprite.getPosition();
    sprite.setPosition(sprite.getPosition() + Velocity * gameTime.asSeconds());
    AnimatingObject::Update(gameTime);
}

void Player::collisionPosition(sf::FloatRect otherHitbox)
{
    bool isColliding = getHitbox().intersects(otherHitbox);

    if (isColliding) 
    {   
        sf::Vector2f newPosition = sprite.getPosition();
        
        sf::Vector2f depth = CalculateCollisionDepth(otherHitbox);
        if (std::abs(depth.x) < std::abs(depth.y))
        {
            newPosition.x -= depth.x;
        }
        else
        {
            newPosition.y -= depth.y;
        }
        sprite.setPosition(newPosition);
    }
}

sf::Vector2f Player::setPosition(sf::Vector2f(newPosition))
{
    sprite.setPosition(newPosition);
    return sprite.getPosition();
}

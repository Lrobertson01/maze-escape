#pragma once
#include "SpriteObject.h"
#include "Asset_Manager.h"
#include "AnimatingObject.h"
class Player :
    public AnimatingObject
{
    //constructor
public:
    Player(sf::Vector2f startingPos);
    void input();
    void update(sf::Time gameTime);
    void collisionPosition(sf::FloatRect otherHitbox);
    sf::Vector2f setPosition(sf::Vector2f(newPosition));
private:
    sf::Vector2f previousPosition;
};


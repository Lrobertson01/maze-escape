#include "Level.h"

Level::Level(sf::Vector2f startingPos)
	:playerObject(startingPos)
	,wallInstances()
{
	
}

void Level::input()
{
	playerObject.input();
}

void Level::update(sf::Time gameTime)
{
	playerObject.update(gameTime);
	for (int i = 0; i < wallInstances.size(); i++) 
	{
		playerObject.collisionPosition(wallInstances[i].getHitbox());
	}
}

void Level::DrawTo(sf::RenderTarget& target)
{
	playerObject.DrawTo(target);
	for (int i = 0; i < wallInstances.size(); i++)
	{
		wallInstances[i].DrawTo(target);
	}
}

void Level::LoadLevel(int levelNumber)
{
	wallInstances.clear();
	std::string filePathPart1 = "Assets/Levels/Level";
	std::string filePathPart2 = std::to_string(levelNumber);
	std::string filePathPart3 = ".txt";

	std::string fullFilePath = filePathPart1 + filePathPart2 + filePathPart3;

	std::ifstream inFile;
	inFile.open(fullFilePath);

	if (!inFile)
	{
		std::cerr << "Unable to open file" + fullFilePath;
		exit(1);
	}

	char ch;
	float x = 0.0f;
	float y = 0.0f;

	float xSpacing = 100.0f;
	float ySpacing = 100.0f;

	while (inFile >> std::noskipws >> ch) 
	{
		if (ch == ' ')
		{
			// New column, increase x
			x += xSpacing;
		}
		else if (ch == '\n')
		{
			// New row, increase y and set x back to 0
			x = 0.0f;
			y += ySpacing;
		}
		else if (ch == 'P')
		{
			// Player(positioning the player)
			playerObject.setPosition(sf::Vector2f(x, y));
		}
		else if (ch == 'W')
		{
			// Walls(creating them, positioning them, and adding them to the vector)
			wallInstances.push_back(Wall(sf::Vector2f(x, y)));
		}
		else if (ch == '-')
		{
			// Do nothing - empty space
		}
		else
		{
			std::cerr << "Unrecognised character in level file: " << ch;
		}
	}

	inFile.close();
}



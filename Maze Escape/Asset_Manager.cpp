#include "Asset_Manager.h"

std::map<std::string, sf::Texture> AssetManager::textures = std::map<std::string, sf::Texture>();
std::map<std::string, sf::SoundBuffer> AssetManager::sounds= std::map<std::string, sf::SoundBuffer>();
std::map<std::string, sf::Font> AssetManager::fonts = std::map<std::string, sf::Font>();

sf::Texture& AssetManager::RequestTexture(std::string fileName)

{

    // TODO: insert return statement here
    auto pairFound = textures.find(fileName);
    if (pairFound != textures.end())
    {
        return pairFound->second;
    }
    else 
    {
        sf::Texture& newTexture = textures[fileName];
        newTexture.loadFromFile(fileName);
        return newTexture;
    }

}

sf::SoundBuffer& AssetManager::RequestSound(std::string fileName)
{
    auto pairFound = sounds.find(fileName);
    if (pairFound != sounds.end())
    {
        return pairFound->second;
    }
    else
    {
        sf::SoundBuffer& newSound = sounds[fileName];
        newSound.loadFromFile(fileName);
        return newSound;
    }
}

sf::Font& AssetManager::RequestFont(std::string fileName)
{
    // TODO: insert return statement here
    auto pairFound = fonts.find(fileName);
    if (pairFound != fonts.end())
    {
        return pairFound->second;
    }
    else
    {
        sf::Font& newFonts = fonts[fileName];
        newFonts.loadFromFile(fileName);
        return newFonts;
    }

}

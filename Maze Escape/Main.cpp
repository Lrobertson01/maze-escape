#include <SFML/Graphics.hpp>
#include "Asset_Manager.h"
#include "Level.h"

int main()
{
    sf::RenderWindow GameWindow(sf::VideoMode::getDesktopMode(), "Maze Escape!", sf::Style::Titlebar | sf::Style::Close);

    Level testLevel(sf::Vector2f(GameWindow.getSize().x / 2, GameWindow.getSize().y / 2));
    testLevel.LoadLevel(2);
    

    
    sf::Clock gameClock;

    while (GameWindow.isOpen())
    {
        sf::Event event;
        while (GameWindow.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                GameWindow.close();
        }

        //Input
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
        {
            //Add an easy way to exit the game
            GameWindow.close();
        }
        testLevel.input();

        //Update
        sf::Time frameTime = gameClock.restart();
        
        testLevel.update(frameTime);
        
        

        GameWindow.clear();
        testLevel.DrawTo(GameWindow);
        
        GameWindow.display();
    }

    return 0;
}
#pragma once
#include "SpriteObject.h"
#include <map>
class AnimatingObject :
    public SpriteObject
{

public:
    AnimatingObject(sf::Texture& newTexture, int newFrameWidth, int newFrameHeight, float newFPS);
    void Update(sf::Time GameTime);
    void addClip(std::string name, int StartFrame, int EndFrame);
    void PlayClip(std::string name);
    void Pause();
    void Stop();
    void Resume();



private:
    int frameWidth;
    int frameHeight;
    float FPS;
    int currentFrame;
    sf::Time timeinFrame;
    bool playing;
    struct Clip
    {
    public:
        int startFrame;
        int endFrame;

    };

    void UpdateSpriteTextureRect();

    std::map<std::string, Clip> Clips;
    std::string currentClip;
};

